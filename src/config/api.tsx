import {Platform} from 'react-native';
import ENV from 'react-native-config';

export const PROTOCOL = () => {
  if (Platform.OS === 'android' && Platform.Version < 22) {
    return 'http://';
  }
  return 'https://';
};

export const BASE_URL = ENV.BASE_URL;
export const API_KEY = ENV.API_KEY;
